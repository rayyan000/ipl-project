package com.rxyyxn;

public class Match {
    private String id,season,city,date,team1,team2,toss_winner,
            toss_decision,result,dl_applied,winner,win_by_runs,
            win_by_wickets,player_of_match,venue,umpire1,umpire2,umpire3;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSeason() {
        return season;
    }
    public void setSeason(String season) {
        this.season = season;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getTeam1() {
        return team1;
    }
    public void setTeam1(String team1) {
        this.team1 = team1;
    }
    public String getTeam2() {
        return team2;
    }
    public void setTeam2(String team2) {
        this.team2 = team2;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String team2) {
        this.date = date;
    }
    public String getTossWinner() {
        return toss_winner;
    }
    public void setTossWinner(String toss_winner) {
        this.toss_winner = toss_winner;
    }
    public String getTossDecision() {
        return toss_decision;
    }
    public void setTossDecision(String toss_decision) {
        this.toss_decision = toss_decision;
    }
    public String getResult() { return result; }
    public void setResult(String result) {
        this.result = result;
    }
    public String getDLApplied() {
        return dl_applied;
    }
    public void setDLApplied(String dl_applied) {
        this.dl_applied = dl_applied;
    }
    public String getWinner() { return winner; }
    public void setWinner(String winner) {
        this.winner = winner;
    }
    public String getWinByRuns() {
        return win_by_runs;
    }
    public void setWinByRuns(String win_by_runs) {this.win_by_runs = win_by_runs;}

    public String getWinByWickets() { return win_by_wickets; }
    public void setWinByWickets(String win_by_wickets) {this.win_by_wickets = win_by_wickets;}
    public String getPlayerOfMatch() {return player_of_match;}
    public void setPlayerOfMatch(String player_of_match) {this.player_of_match = player_of_match;}
    public String getVenue() {return venue;}
    public void setVenue(String team2) {this.venue = venue;}
    public String getFirstUmpire() {
        return umpire1;
    }
    public void setFirstUmpire(String umpire1) {
        this.umpire1 = umpire1;
    }
    public String getSecondUmpire() {
        return umpire2;
    }
    public void setSecondUmpire(String player_of_match) {
        this.umpire2 = umpire2;
    }
    public String getThirdUmpire() {
        return umpire3;
    }
    public void setThirdUmpire(String team2) {
        this.umpire3 = umpire3;
    }
}
