package com.rxyyxn;

import java.io.*;
import java.util.*;

import static java.lang.Thread.sleep;

public class Main {
    public static final int MATCH_ID = 0;
    public static final int MATCH_SEASON = 1;
    public static final int MATCH_CITY = 2;
    public static final int MATCH_DATE = 3;
    public static final int MATCH_TEAM_1 = 4;
    public static final int MATCH_TEAM_2 = 5;
    public static final int MATCH_TOSS_WINNER = 6;
    public static final int MATCH_TOSS_DECISION = 7;
    public static final int MATCH_TOSS_RESULT = 8;
    public static final int MATCH_DL_APPLIED_OR_NOT = 9;
    public static final int MATCH_WINNING_TEAM_OF_PLAYERS = 10;
    public static final int MATCH_WINNING_MARGIN = 11;
    public static final int MATCH_WINNING_WICKET_MARGIN = 12;
    public static final int MAN_OF_D_MATCH = 13;
    public static final int MATCH_VENUE = 14;
    public static final int FIRST_UMPIRE = 15;
    public static final int SECOND_UMPIRE = 16;
    public static final int THIRD_UMPIRE = 17;
    public static final int DELIVERY_MATCH_ID = 0;
    public static final int DELIVERY_INNINGS = 1;
    public static final int DELIVERY_BATTING_TEAM = 2;
    public static final int DELIVERY_BOWLING_TEAM = 3;
    public static final int DELIVERY_OVER = 4;
    public static final int DELIVERY_BALL = 5;
    public static final int DELIVERY_BATSMAN = 6;
    public static final int DELIVERY_NON_STRIKER = 7;
    public static final int DELIVERY_BOWLER = 8;
    public static final int DELIVERY_IS_IT_A_SUPER_OVER = 9;
    public static final int DELIVERY_WIDE_RUNS = 10;
    public static final int DELIVERY_BYE_RUNS = 11;
    public static final int DELIVERY_LEG_BYE_RUNS = 12;
    public static final int DELIVERY_NO_BALL_RNS = 13;
    public static final int DELIVERY_PENALTY_RUNS = 14;
    public static final int DELIVERY_BATSMAN_RUNS = 15;
    public static final int DELIVERY_EXTRA_RUNS = 16;
    public static final int DELIVERY_TOTAL_RUNS = 17;
    public static final int DELIVERY_PLAYER_DISMISSED = 18;
    public static final int DELIVERY_TYPEOF_DISMISSED = 19;
    public static final int DELIVERY_FIELDER_DATA = 20;

    public static void main(String[] args) throws Exception {
        List<Match> matches = getMatchesData();
        List<Delivery> deliveries = getDeliveriesData();
        findNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWonByEachTeam(matches);
        find2016ExtrasPerTeam(matches, deliveries);
        findTheMostEconomicalBowlerIn2015(matches, deliveries);
        fifthFunction(matches);
    }

    private static void find2016ExtrasPerTeam(List<Match> matches, List<Delivery> deliveries) throws InterruptedException {
        Thread.sleep(2000);
        HashMap<String, Integer> result = new HashMap<>();
        int extrasConceded;
        Vector<Integer> requiredMatchIDs = new Vector<>();
        for(Match element : matches) {
            if(element.getSeason().equals("2016"))
                requiredMatchIDs.add(Integer.parseInt(element.getId()));
        }
        for(Delivery element : deliveries) {
            for(Integer identification : requiredMatchIDs) {
                if(identification == Integer.parseInt(element.getID())) {
                    String theBowlingTeam = element.getBowlingTeam();
                    extrasConceded = Integer.parseInt(element.getExtraRuns());
                    if(!result.containsKey(theBowlingTeam))
                        result.put(theBowlingTeam,extrasConceded);
                    else result.put(theBowlingTeam,result.get(theBowlingTeam)+extrasConceded);
                }
            }
        }
        System.out.println("\nIn the year 2016, the extra runs conceded per team are:\n" + result + "\n");
    }
    private static void findNumberOfMatchesWonByEachTeam(List<Match> matches) throws InterruptedException {
        Thread.sleep(2000);
        HashMap<String, Integer> result = new HashMap<>();
        String teamName;
        int matchesWon;
        for(Match element : matches) {
            String nameOfTeam = element.getWinner();
            if(!result.containsKey(nameOfTeam))
                result.put(nameOfTeam,1);
            else result.put(nameOfTeam,result.get(nameOfTeam)+1);
        }
        System.out.println("\nNumber of matches won by each team over all the years:\n" + result);
    }
    private static void findTheMostEconomicalBowlerIn2015(List<Match> matches, List<Delivery> deliveries) throws Exception {
        Thread.sleep(2000);
        HashMap<String, Float> bowlerVsRuns = new HashMap<>();
        HashMap<String, Float> bowlerVsOversBowled = new HashMap<>();
        HashMap<String, Float> bowlerVsBallsBowled = new HashMap<>();
        HashMap<String, Float> bowlerVsEconomy = new HashMap<>();
        Vector<Integer> requiredMatchIDs = new Vector<>();
        for(Match element : matches) {
            if(element.getSeason().equals("2015"))
                requiredMatchIDs.add(Integer.parseInt(element.getId()));
        }
        for(Delivery element : deliveries) {
            for(Integer identification : requiredMatchIDs) {
                if(identification == Integer.parseInt(element.getID())) {
                    String theBowlersName = element.getBowler();
                    Float runsConceded = (float) Integer.parseInt(element.getTotalRuns());
                    if(!bowlerVsRuns.containsKey(theBowlersName))   {
                        bowlerVsRuns.put(theBowlersName, runsConceded);
                        bowlerVsBallsBowled.put(theBowlersName, 1F);
                    }
                    else {
                        bowlerVsRuns.put(theBowlersName, bowlerVsRuns.get(theBowlersName) + (float) runsConceded);
                        bowlerVsBallsBowled.put(theBowlersName,bowlerVsBallsBowled.get(theBowlersName) + 1F);
                    }
                }
            }
        }
        for (HashMap.Entry<String,Float> mapElement : bowlerVsBallsBowled.entrySet()) {
            String bowler = (mapElement.getKey());
            Float bowls = mapElement.getValue();
            if(!bowlerVsOversBowled.containsKey(bowler))
                bowlerVsOversBowled.put(bowler,bowls/6);
        }
        for (HashMap.Entry<String,Float> mapElement : bowlerVsOversBowled.entrySet()) {
            String theBowlersName = (mapElement.getKey());
            Float oversBowledByTheBowler = mapElement.getValue();
            if(!bowlerVsEconomy.containsKey(theBowlersName))
                bowlerVsEconomy.put(theBowlersName,((bowlerVsRuns.get(theBowlersName))/oversBowledByTheBowler));
        }
        HashMap<String, Integer> sorted;
        List<Map.Entry<String, Float> > listOfBowlerVsEconomies = new LinkedList<Map.Entry<String, Float> >(bowlerVsEconomy.entrySet());
        listOfBowlerVsEconomies.sort(new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        HashMap<String, Float> sortedOfBowlersVsEconomies = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> element : listOfBowlerVsEconomies) {
            sortedOfBowlersVsEconomies.put(element.getKey(), element.getValue());
        }
        System.out.println("In the year 2015, the economy of the bowlers are:\n");
        for (Map.Entry<String, Float> entry : sortedOfBowlersVsEconomies.entrySet())
            System.out.println("Bowler = " + entry.getKey() + ", Economy = " + entry.getValue());

    }

    private static void fifthFunction(List<Match> matches) throws Exception {
        Thread.sleep(2000);
        System.out.println("\nEnter match ID to get the resultant match winning team and the resultant best player selected by authorities of that special cricketing competition: ");
        Scanner myObj = new Scanner(System.in);
        int matchID = myObj.nextInt(), attempts =3;
        if(matchID < 1 || matchID > 636) {
            do {
                System.out.println("Enter ID between 1 and 636 only, as records are only between these numbers. . . ");
                myObj = new Scanner(System.in);
                matchID = myObj.nextInt();
                attempts--;
                if(attempts == 0) {
                    System.out.println("Bruh.");
                    break;
                }
            } while(matchID < 1 || matchID > 636);
        }
        if(attempts == 0)
            System.out.println("I said about ID between 1 and 636");
        System.out.println("Match ID entered is: " + matchID);
        for(Match element : matches) {
            int id = Integer.parseInt(element.getId());
            if(id == matchID) {
                String manOfTheMatch = element.getPlayerOfMatch();
                String matchWinner = element.getWinner();
                System.out.println("Match won by " + matchWinner + "\nBest Player selected by special authorities present: " + manOfTheMatch);
                break;
            }
        }
    }
    private static void findNumberOfMatchesPlayedPerYear(List<Match> matches) throws Exception {
        Thread.sleep(2000);
        HashMap<Integer, Integer> result = new HashMap<>();
        for(int year = 2008; year <2018; year++) {
            int counter = 0;
            for(Match element : matches) {
                int currentYear = Integer.parseInt(element.getSeason());
                if(currentYear == year)
                    counter++;
            }
            result.put(year,counter);
        }
        System.out.println("Number of matches played per year:\n" + result);
        }

    private static List<Delivery> getDeliveriesData() {
        boolean headerRowHandling = true;
        String line = null;
        List<Delivery> deliveries = new ArrayList<>();
        File fileMac = new File("/home/user/Downloads/IPL-Project/deliveries.csv");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileMac));
            while((line = reader.readLine())!=null) {
                if(headerRowHandling) {
                    headerRowHandling = false;
                    continue;
                }
                String[] data = line.split(",");
                Delivery delivery = new Delivery();
                delivery.setId(data[DELIVERY_MATCH_ID]);
                delivery.setInningNumber(data[DELIVERY_INNINGS]);
                delivery.setBattingTeam(data[DELIVERY_BATTING_TEAM]);
                delivery.setBowlingTeam(data[DELIVERY_BOWLING_TEAM]);
                delivery.setOver(data[DELIVERY_OVER]);
                delivery.setBall(data[DELIVERY_BALL]);
                delivery.setBatsman(data[DELIVERY_BATSMAN]);
                delivery.setNonStriker(data[DELIVERY_NON_STRIKER]);
                delivery.setBowler(data[DELIVERY_BOWLER]);
                delivery.setIsSuperOver(data[DELIVERY_IS_IT_A_SUPER_OVER]);
                delivery.setWideRuns(data[DELIVERY_WIDE_RUNS]);
                delivery.setByeRuns(data[DELIVERY_BYE_RUNS]);
                delivery.setLegByeRuns(data[DELIVERY_LEG_BYE_RUNS]);
                delivery.setNoBallRuns(data[DELIVERY_NO_BALL_RNS]);
                delivery.setPenaltyRuns(data[DELIVERY_PENALTY_RUNS]);
                delivery.setBatsmanRuns(data[DELIVERY_BATSMAN_RUNS]);
                delivery.setExtraRuns(data[DELIVERY_EXTRA_RUNS]);
                delivery.setTotalRuns(data[DELIVERY_TOTAL_RUNS]);
                deliveries.add(delivery);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    private static List<Match> getMatchesData() {
        boolean headerRowHandling = true;
        String line = null;
        List<Match> matches = new ArrayList<>();
        File fileMac = new File("/home/user/Downloads/IPL-Project/matches.csv");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileMac));
            while((line = reader.readLine())!=null) {
                if(headerRowHandling) {
                    headerRowHandling = false;
                    continue;
                }
                String[] data = line.split(",");
                Match match = new Match();
                match.setId(data[MATCH_ID]);
                match.setSeason(data[MATCH_SEASON]);
                match.setCity(data[MATCH_CITY]);
                match.setDate(data[MATCH_DATE]);
                match.setTeam1(data[MATCH_TEAM_1]);          
                match.setTeam2(data[MATCH_TEAM_2]);
                match.setTossWinner(data[MATCH_TOSS_WINNER]);
                match.setTossDecision(data[MATCH_TOSS_DECISION]);   
                match.setResult(data[MATCH_TOSS_RESULT]);
                match.setDLApplied(data[MATCH_DL_APPLIED_OR_NOT]);      
                match.setWinner(data[MATCH_WINNING_TEAM_OF_PLAYERS]);
                match.setWinByRuns(data[MATCH_WINNING_MARGIN]);
                match.setWinByWickets(data[MATCH_WINNING_WICKET_MARGIN]);
                match.setPlayerOfMatch(data[MAN_OF_D_MATCH]);
                match.setVenue(data[MATCH_VENUE]);
                matches.add(match);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return matches;
    }
}